function countLetter(letter, sentence) {
    let result = 0;

if (letter.length === 1) {
      for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
          result++;
        }
      }
      return result;
    } else {
      return undefined;
    }
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

}



function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    text = text.toLowerCase();
  
    for (let i = 0; i < text.length; i++) {
        if (text.indexOf(text[i]) !== i) {
            return false;
        }
    }
    return true; 
}



function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if (age < 13) {
        return undefined;
    }

//     if (age >= 13 && age <= 21 || age >= 65) {
//         let discountedPrice = price * 0.8;
//         return Math.round(discountedPrice + Number.EPSILON).toString();
//     }
  
//     // Return the rounded off price for people aged 22 to 64.
//     return Math.round(price + Number.EPSILON).toString();
// }

    if (age >= 13 && age <= 21 || age >= 65) {
        let discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);
    }
  
    // Return the rounded off price for people aged 22 to 64.
    return price.toFixed(2);
}




function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let hotCategories = {};

    // Loop through each item in the array
    for (let i = 0; i < items.length; i++) {
        let item = items[i];

        // Check if the current item has run out of stock
        if (item.stocks === 0) {
            // If the category of the current item is not yet in the hotCategories object, add it
            if (!hotCategories[item.category]) {
                hotCategories[item.category] = true;
            }
        }
    }

    // Return the keys of the hotCategories object as an array
    return Object.keys(hotCategories);
}




function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    let votedForA = {};

    // Loop through each voter in candidate A's array and add them to the votedForA object
    for (let i = 0; i < candidateA.length; i++) {
        votedForA[candidateA[i]] = true;
    }

    // Initialize an empty array to store the voters who voted for both candidates
    let flyingVoters = [];

    // Loop through each voter in candidate B's array and check if they also voted for candidate A
    for (let i = 0; i < candidateB.length; i++) {
        if (votedForA[candidateB[i]]) {
            // If the current voter also voted for candidate A, add them to the flyingVoters array
            flyingVoters.push(candidateB[i]);
        }
    }

    // Return the flyingVoters array
    return flyingVoters;
}




module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};